### PRODUCTION V1.0
### GRPC Setup pocedure

### STEP-1-----------------------------------------------------------------------------
# #############################################################################
# 
# INSTALL BREW and Initialize Protobuf
# 
# #############################################################################
## install homebrew or linuxbrew
sudo apt-get install linuxbrew

## then install protobuf compile as follow from whitin the project directory
brew install protobuf
brew install clang-format

## Install the protocol compiler plugins for Go using the following commands:
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

### STEP-2-----------------------------------------------------------------------------
# #############################################################################
# 
# CONFIGURE THE ENVIRONMENT
# 
# #############################################################################

### Install the protocol buffer compler if not exist
https://grpc.io/docs/protoc-installation/

### Update your PATH so that the protoc compiler can find the plugins:

export PATH="$PATH:$(go env GOPATH)/bin"

## Got grpc.io and install 2 Go libraries
go get -u google.golang.org/grpc
go get -u github.com/go/golang/protobuf/protoc-gen-go
go get -u github.com/go/golang/protobuf/protoc-gen-go-grpc

## create in your project directory the two subdirectories pb and proto 
## proto: holds the message definition from which the api is created
## pb: holds the created langange file (go, c,java or kothlin) to be later compile 

protoc --proto_path=proto proto/*.proto --go_out=plugins=grpc:pb

# you can change the proto path from the VSCODE proto extension setting by chosing the extension and going to setting and finding protoc

### STEP-3-----------------------------------------------------------------------------
# #############################################################################
# 
# GENERATING AN API from gRpc ECHOSERVER
# 
# #############################################################################
code generation from

create a tools.go file in the main project directory
touch tools.go
add the following ontent to the file
/*
// +build tools

package tools

import (
    _ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway"
    _ "github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2"
    _ "google.golang.org/grpc/cmd/protoc-gen-go-grpc"
    _ "google.golang.org/protobuf/cmd/protoc-gen-go"
)
*/

then run the command below to pull the librairies

go mod tidy



### STEP-4-----------------------------------------------------------------------------
# #############################################################################
# 
# RUNNING THE ECHOSERVER
# 
# #############################################################################

###  to run the echoserver goto the server directory and start it and do the same for the clien
cd server/
go run .

cd ../client
go run

## that's it
