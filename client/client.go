package main

import (
	"context"
	pb "echoserver/gen/proto"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"google.golang.org/grpc"
)

func main() {

	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file:", err)
	}
	// Access the loaded environment variables
	// apiKey := os.Getenv("API_KEY")
	activProfile := os.Getenv("ACTIV_PROFILE")
	//clientProtocol := os.Getenv("CLIENT_PROTOCOL")
	srvHost := os.Getenv("SERVER_HOST")
	srvPort := os.Getenv("SERVER_PORT")

	fmt.Println("Active Profile: ", activProfile)
	fmt.Println("ServerUrl: ", srvHost + ":" + srvPort)
	

	conn, err := grpc.Dial(srvHost+":"+srvPort, grpc.WithInsecure())
	if err != nil {
		log.Println(err)
	}

	client := pb.NewEchoApiClient(conn)

	resp, err := client.Echo(context.Background(), &pb.ResponseRequest{Msg: "Hello Everyone out there!"})
	if err != nil {
		log.Println(err)
	}

	fmt.Println(resp)
	fmt.Println(resp.Msg)
}