create:
	protoc --proto_path=proto proto/*.proto --go_out=gen/ 
	protoc --proto_path=proto proto/*.proto --go-grpc_out=gen/ 
	protoc -I . --grpc-gateway_out ./gen/ \
	    --grpc-gateway_opt logtostderr=true \
		--grpc-gateway_opt paths=source_relative \
		--grpc-gateway_opt generate_unbound_methods=true \
		proto/echoserver.proto

clean:
	rm gen/proto/*.go
	
run:
	go run main.go

# mkdir -p google/api 
# curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/annotations.proto > proto/google/api/annotations.proto 
# curl https://raw.githubusercontent.com/googleapis/googleapis/master/google/api/http.proto > proto/google/api/http.protoprotoc -I . --grpc-gateway_out=/gen/ \ 
# protoc -I . --grpc-gateway_out=/gen/ \ 
# 	--grpc-gateway_opt paths=source_relative \ 
# 	--grpc-gateway_opt generate_unbound_methods=true \
#     proto/echoserver.proto
