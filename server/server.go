package main

import (
	"context"
	pb "echoserver/gen/proto"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
)

type EchoApiServer struct {
	pb.UnimplementedEchoApiServer
} 

func (s *EchoApiServer) Echo(ctx context.Context, req *pb.ResponseRequest) (*pb.ResponseRequest, error) {
	return req, nil
}

func (s *EchoApiServer) GetUser(ctx context.Context, req *pb.UserRequest) (*pb.UserResponse, error) {
	return &pb.UserResponse{}, nil
}

func main() {

	// Load environment variables from .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file:", err)
	}
	// Access the loaded environment variables
	// apiKey := os.Getenv("API_KEY")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	activProfile := os.Getenv("ACTIV_PROFILE")
	srvProtocol := os.Getenv("SERVER_PROTOCOL")
	srvHost := os.Getenv("SERVER_HOST")
	srvPort := os.Getenv("SERVER_PORT")

	pubHost := os.Getenv("PUBLIC_HOST")
	pubPort := os.Getenv("PUBLIC_PORT")

	fmt.Println("Active Profile: ", activProfile)
	fmt.Println("ServerUrl: ", srvHost + ":" + srvPort)
	fmt.Println("DatabaseUri: ", dbHost + ":" + dbPort)

    //This aims at lunching the http/https server for universal API
	go func() {
		// tree importants thinks TODO in order to lunch an http server in paralele
		//1- mux
		gwmux := runtime.NewServeMux()
		//2- register
		pb.RegisterEchoApiHandlerServer(context.Background(), gwmux, &EchoApiServer{})
		//3- run the http server
		log.Fatalln(http.ListenAndServe(pubHost + ":" + pubPort, gwmux))
	}()
	// create a listener instance
	listner, err := net.Listen(srvProtocol, srvHost + ":" + srvPort )
	if err != nil {
		log.Fatalln(err)
	}
	//create a sever instance
	grpcServer := grpc.NewServer()
	pb.RegisterEchoApiServer(grpcServer, &EchoApiServer{})

	//intanciate the server and let it listen to the defined tcp protocol and porte
	err = grpcServer.Serve(listner)
	if err != nil {
		log.Println(err)
	}
}
